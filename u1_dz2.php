 <?php

function findDate(DateTimeInterface $inputDate, array $weekdays)
{
	$weekdays = array_flip($weekdays);

	foreach (new DatePeriod($inputDate, new DateInterval('P1D'), 7, DatePeriod::EXCLUDE_START_DATE) as $possibleDate) {
		if (isset($weekdays[$possibleDate->format('w')]) === true){
		return $possibleDate;
		}
	}
}

$weekdays = [1, 6];
$inputDate = '2021/01/01';

$offerDate = findDate(new DateTimeImmutable($inputDate), $weekdays);

$end = new DateTime("30.12.2021");
$step = new DateInterval('P7D');
$period = new DatePeriod($offerDate, $step, $end);

foreach($period as $datetime) {
	if ($datetime->format("j") <= 20){
		echo $datetime->format("j.m.Y") . "<br />";
	}
}

