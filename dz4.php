<?php

abstract class Animals
{
    abstract public function action();
    
    public function __construct($voice)
    {
        $this->voice = $voice;
    }

    public function voice()
    {
        return print_r(' и говорит ' . $this->voice);
    }   
}

class Cat extends Animals
{
	public function nameAnimal()
    {
        return print_r('Кошка ');
    }
	
    public function action()
    {
        return print_r('ловит мышей ');
    }
    
    public function __construct()
    {
        parent::__construct('Мяу!<br>');
    }
}

class Dog extends Animals
{
    public function nameAnimal()
    {
        return print_r('Собака ');
    }
	
    public function action()
    {
        return print_r('охраняет дом ');
    }
    
    public function __construct()
    {
        parent::__construct('Гав!<br>');
    }
}

class Сow extends Animals
{
	public function nameAnimal()
    {
        return print_r('Корова ');
    }
	
    public function action()
    {
        return print_r('дает молоко');
    }
    
    public function __construct()
    {
        parent::__construct('Му!<br>');
    }
}

$cat = new Cat();
$dog = new Dog();
$cow = new Сow();
$cat->nameAnimal();
$cat->action();
$cat->voice();
$dog->nameAnimal();
$dog->action();
$dog->voice();
$cow->nameAnimal();
$cow->action();
$cow->voice();
