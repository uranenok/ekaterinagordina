<?php

echo 'Домашнее задание 1: <br>';

$a = [1337, 3, 6, 1, 3, 34, 686, 34, 41, 78, 10, 17];

$length = count($a);

for ($i = 0; $i < $length - 1; $i++) {
    for ($j = 0; $j < $length - 1 - $i; $j++) {
        if ($a[$j] > $a[$j+1]) {
            $k = $a[$j];
            $a[$j] = $a[$j + 1];
            $a[$j + 1] = $k;
        }
    }
}

foreach ($a as $row) {
    echo $row . " ";
} 

echo '<br><br><br>Домашнее задание 2: <br>';

$length = 40;

for($i = 0; $i < $length; $i++) {
    $arr1[$i] = rand(-20, 20);
}

for($i = 0; $i < $length; $i++) {
    $arr2[$i] = rand(-20, 20);
}

echo 'Первый массив: <br>';

foreach ($arr1 as $row) {
    echo $row . " ";
}

echo '<br>Второй массив:<br>';

foreach ($arr2 as $row) {
    echo $row . " ";
}

echo '<br>Результат:<br>';

for($i = 2, $j = 1; $i <= $length; $i = ($i + 3), $j++) {
    if ($arr1[$i] > $arr2[$i - $j]) {
        echo $arr1[$i] . ' больше ' . $arr2[$i - $j] . '<br>';
    }
    elseif ($arr1[$i] < $arr2[$i - $j]) {
        echo $arr1[$i] . ' меньше ' . $arr2[$i - $j] . '<br>';
    }
    else {
        echo $arr1[$i] . ' равно ' . $arr2[$i - $j] . '<br>';
    }
}


