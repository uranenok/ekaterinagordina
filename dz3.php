<?php $connect = mysqli_connect('localhost', 'root', '', 'taxi'); ?>


<style>
    th, td {
        padding: 10px;
    }
    
    th {
        background: #b5b5b5;
        color: #fff;
    }
    
    td {
        background: #DCDCDC;
    }
</style>

<table>
    <tr>
        <th>Дата заказа</th>
        <th>Начальная точка маршрута</th>
        <th>Конечная точка маршрута</th>
        <th>Номер автомобиля</th>
        <th>ФИО водителя</th>
        <th>ФИО клиента</th>        
        <th>Название тарифа</th>
        <th>Стоимость</th>
    </tr>

	<?php

	$taxi = mysqli_query($connect, 
		"SELECT orders.date AS 'Дата заказа', orders.start_route AS 'Начальная точка маршрута', orders.end_route AS 'Конечная точка маршрута', drivers.car_number AS 'Номер автомобиля', CONCAT(drivers.last_name, ' ', drivers.name, ' ', drivers.patronymic) AS 'ФИО водителя', CONCAT(users.last_name, ' ', users.name, ' ', users.patronymic) AS 'ФИО клиента', tariffs.name AS 'Название тарифа', tariffs.price AS 'Стоимость' 
			FROM orders     
				INNER JOIN drivers ON orders.driver_id = drivers.id 
					INNER JOIN users ON orders.user_id = users.id 
						INNER JOIN tariffs ON orders.tariff_id = tariffs.id;");
					
	$taxi = mysqli_fetch_all($taxi);
	
	foreach ($taxi as $line_taxi) {
		echo '<tr>';
		
		for ($i = 0; $i < 8; $i++) {
			echo '<td>' . $line_taxi[$i] . '</td>';
		}
		
		echo '</tr>';       
	} 
		
	?>
    
</table> 
