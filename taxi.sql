-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 18 2022 г., 16:35
-- Версия сервера: 5.7.33
-- Версия PHP: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `taxi`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cars`
--

CREATE TABLE `cars` (
  `id` bigint(100) NOT NULL,
  `number` tinytext,
  `color` tinytext,
  `model` tinytext,
  `class` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `cars`
--

INSERT INTO `cars` (`id`, `number`, `color`, `model`, `class`) VALUES
(1, 'В214СР13', 'белый', 'Ford Fusion', 'B'),
(2, 'У351КС13', 'синий', 'Audi A3', 'С'),
(3, 'В125ПК90', 'черный', 'Fiat Punt', 'B'),
(4, 'М777ММ777', 'черный', 'Mercedes E-Class', 'E'),
(5, 'К351ВР13', 'желтый', 'Opel Astra', 'С');

-- --------------------------------------------------------

--
-- Структура таблицы `drivers`
--

CREATE TABLE `drivers` (
  `id` bigint(20) NOT NULL,
  `last_name` tinytext,
  `name` tinytext,
  `patronymic` tinytext,
  `experience` tinytext,
  `category` text,
  `date_of_employment` date NOT NULL,
  `car_number` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `drivers`
--

INSERT INTO `drivers` (`id`, `last_name`, `name`, `patronymic`, `experience`, `category`, `date_of_employment`, `car_number`) VALUES
(1, 'Иванов', 'Иван', 'Иванович', '7', 'B', '2020-05-17', 'В214СР13'),
(2, 'Сидоров', 'Петр', 'Сергеевич', '15', 'B', '2010-01-24', 'У351КС13'),
(3, 'Петров', 'Василий', 'Александрович', '10', 'B', '2016-05-10', 'В125ПК90'),
(4, 'Иванов', 'Михаил', 'Сергеевич', '11', 'B', '2014-10-12', 'В214СР13'),
(5, 'Смирнов', 'Иван', 'Иванович', '8', 'B', '2018-12-01', 'К351ВР13'),
(6, 'Пароходов', 'Денис', 'Федорович', '7', 'B', '2021-04-28', 'В214СР13'),
(7, 'Моисеев', 'Сергей', 'Андреевич', '13', 'B', '2014-05-15', 'К351ВР13'),
(8, 'Кулаков', 'Семен', 'Николаевич', '20', 'B', '2016-08-01', 'М777ММ777'),
(9, 'Уваков', 'Рустам', 'Шамильевич', '18', 'B', '2019-07-07', 'У351КС13'),
(10, 'Ушкин', 'Александр', 'Александрович', '9', 'B', '2022-03-18', 'В125ПК90');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `driver_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `start_route` tinytext,
  `end_route` tinytext,
  `tariff_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `driver_id`, `user_id`, `date`, `start_route`, `end_route`, `tariff_id`) VALUES
(1, 1, 1, '2022-04-13 21:00:00', 'ул. Гагарина, 3', 'ул. Рабочая, 110-68', 3),
(2, 2, 3, '2022-04-13 21:00:00', 'ул. Коваленко, 57', 'ул. Карла Либкнехта, 7', 4),
(3, 5, 4, '2022-04-12 21:00:00', 'ул. Болдина, 18', 'ул. Коммунистическая, 87', 1),
(4, 6, 4, '2022-04-11 21:00:00', 'ул. Республиканская, 123', 'ул. Николаева, 75', 4),
(5, 3, 5, '2022-04-11 21:00:00', 'ул. Серафимовича, 9', 'ул. Масловского, 17', 3),
(6, 4, 2, '2022-04-09 21:00:00', 'Александровское ш., 43А', 'ул. Тани Бибиной, 5', 2),
(7, 1, 1, '2022-04-09 21:00:00', 'ул. Строительная, 21', 'ул. Полежаева, 153', 3),
(8, 1, 3, '2022-04-09 21:00:00', 'пер. Кирпичный, 3', 'ул. Ботевградская, 43а', 5),
(9, 7, 1, '2022-04-08 21:00:00', 'ул. Александра Невского, 50', 'Республиканская ул., 33А', 1),
(10, 10, 2, '2022-04-08 21:00:00', 'ул. Севастопольская, 5', 'пр. 60 лет Октября, 19', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tariffs`
--

CREATE TABLE `tariffs` (
  `id` bigint(20) NOT NULL,
  `name` tinytext,
  `price` decimal(25,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `tariffs`
--

INSERT INTO `tariffs` (`id`, `name`, `price`) VALUES
(1, 'Эконом', '50'),
(2, 'Комфорт', '100'),
(3, 'Комфорт +', '125'),
(4, 'Детский', '120'),
(5, 'Бизнес класс', '200');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `last_name` tinytext NOT NULL,
  `name` tinytext NOT NULL,
  `patronymic` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `last_name`, `name`, `patronymic`) VALUES
(1, 'Савельева', 'Марина', 'Борисовна'),
(2, 'Павлов', 'Юрий', 'Максимович'),
(3, 'Рыбкина', 'Анастасия', 'Валерьевна'),
(4, 'Мишин', 'Антон', 'Сергеевич'),
(5, 'Владимиров', 'Василий', 'Викторович');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tariffs`
--
ALTER TABLE `tariffs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cars`
--
ALTER TABLE `cars`
  MODIFY `id` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `tariffs`
--
ALTER TABLE `tariffs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
